function isInt(value) {
  var intRegex = /^\d+$/;
  return intRegex.test(value);
}

Array.prototype.contains = function (obj) {
 for (i in this) {
   if (this[i] === obj) return true;
 }
 return false;
}

function formatResponses() {
  // Prompts the user for the file name
  var activeSpread = SpreadsheetApp.getActiveSpreadsheet();
  var fileName = activeSpread.getName() + " (Formatted)";

  // Create a file in the Docs List
  var doc = DocumentApp.create(fileName);

  // Get the range in the spreadsheet
  var ws = activeSpread.getDataRange();
  try {
    var data = ws.getValues();

    // add summary heading
    var heading = doc.appendParagraph("Responses Summary");
    heading.setHeading(DocumentApp.ParagraphHeading.HEADING2);

    // Loop through the data in the range and create paragraphs with the appropriate heading
    if (data.length > 1) {
      // Find numerical responses and average them
      var numericColumns = [];
      
      // Find which responses are numeric
      // First column is timestamp, so ignore it
      for (var col = 1; col < data.length; col++) {
        // Only check first row for text/numeric
        if (isInt(data[1][col])) {
          numericColumns.push(col);
        }
      }
      for (col in numericColumns) {
        var sum = 0.0;
        
        // Skip header row
        for (var row = 1; row < data.length; row++) {
          sum = sum + data[row][col];
        }
        var mean = sum / (data.length - 1);

        // Add an response average for numeric responses
        var section = doc.appendParagraph(data[0][col]);
        section.setHeading(DocumentApp.ParagraphHeading.HEADING4);
        doc.appendParagraph("Average response: " + mean);
      }
      
      // Add a summary of each response that isn't numeric
      // First column is timestamp, next is name
      for (var col = 2; col < data[0].length; col++) {
        if (numericColumns.contains(col)) {
          continue;
        }
        
        // Add a header with the question
        var question = doc.appendParagraph(data[0][col]);
        question.setHeading(DocumentApp.ParagraphHeading.HEADING4);

        for (var row = 1; row < data.length; row++) {
          if(data[row][col]!=""){
            var name = data[row][1];
            doc.appendParagraph(name + ": " + data[row][col]);
          }
        }
        
        // Add a page break to end of each row, except for the last one
        doc.appendPageBreak();
      }
    }
  }
  catch(err) {
    Logger.log(err);
    Browser.msgBox(err);
  }
  Browser.msgBox("After clicking \"Ok\", you can view your formatted responses at " + doc.getUrl());
}

/**
 * Adds a custom menu to the active spreadsheet, containing a single menu item
 * for invoking the readRows() function specified above.
 * The onOpen() function, when defined, is automatically invoked whenever the
 * spreadsheet is opened.
 * For more information on using the Spreadsheet API, see
 * https://developers.google.com/apps-script/service_spreadsheet
 */
function onOpen() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var entries = [{
    name : "Format Responses",
    functionName : "formatResponses"
  }];
  spreadsheet.addMenu("Custom Scripts", entries);
};

